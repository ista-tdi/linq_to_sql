﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WinFormsApp7
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="magazin")]
	public partial class DataClasses1DataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertProduit(Produit instance);
    partial void UpdateProduit(Produit instance);
    partial void DeleteProduit(Produit instance);
    #endregion
		
		public DataClasses1DataContext() : 
				base(global::WinFormsApp7.Properties.Settings.Default.magazinConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<Produit> Produits
		{
			get
			{
				return this.GetTable<Produit>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Produit")]
	public partial class Produit : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _REFERENCE;
		
		private string _INTITULE;
		
		private string _CATEGORIE;
		
		private double _PRIX;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnREFERENCEChanging(int value);
    partial void OnREFERENCEChanged();
    partial void OnINTITULEChanging(string value);
    partial void OnINTITULEChanged();
    partial void OnCATEGORIEChanging(string value);
    partial void OnCATEGORIEChanged();
    partial void OnPRIXChanging(double value);
    partial void OnPRIXChanged();
    #endregion
		
		public Produit()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_REFERENCE", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int REFERENCE
		{
			get
			{
				return this._REFERENCE;
			}
			set
			{
				if ((this._REFERENCE != value))
				{
					this.OnREFERENCEChanging(value);
					this.SendPropertyChanging();
					this._REFERENCE = value;
					this.SendPropertyChanged("REFERENCE");
					this.OnREFERENCEChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_INTITULE", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string INTITULE
		{
			get
			{
				return this._INTITULE;
			}
			set
			{
				if ((this._INTITULE != value))
				{
					this.OnINTITULEChanging(value);
					this.SendPropertyChanging();
					this._INTITULE = value;
					this.SendPropertyChanged("INTITULE");
					this.OnINTITULEChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CATEGORIE", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string CATEGORIE
		{
			get
			{
				return this._CATEGORIE;
			}
			set
			{
				if ((this._CATEGORIE != value))
				{
					this.OnCATEGORIEChanging(value);
					this.SendPropertyChanging();
					this._CATEGORIE = value;
					this.SendPropertyChanged("CATEGORIE");
					this.OnCATEGORIEChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_PRIX", DbType="Float NOT NULL")]
		public double PRIX
		{
			get
			{
				return this._PRIX;
			}
			set
			{
				if ((this._PRIX != value))
				{
					this.OnPRIXChanging(value);
					this.SendPropertyChanging();
					this._PRIX = value;
					this.SendPropertyChanged("PRIX");
					this.OnPRIXChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
#pragma warning restore 1591
