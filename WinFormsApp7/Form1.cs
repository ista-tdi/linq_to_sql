﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsApp7
{
    public partial class Form1 : Form
    {

        DataClasses1DataContext context = new DataClasses1DataContext();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            dataGridView1.DataSource = context.Produits;
        }

        private void Ajouter()
        {
            
            Produit newProduit = new Produit();
            newProduit.CATEGORIE = "dewf";
            newProduit.INTITULE = "dewfew";
            newProduit.PRIX = 12;
            newProduit.REFERENCE = 1100;

            context.Produits.InsertOnSubmit(newProduit);
            context.SubmitChanges();
        }


        private void Modfier()
        {

            Produit produit = context.Produits.Single(p => p.REFERENCE == 1);
            
            produit.CATEGORIE = "dewf";
            produit.INTITULE = "dewfew";
            produit.PRIX = 12;

            context.SubmitChanges();
        }


        private void Supprimer()
        {

            Produit produit = context.Produits.Single(p => p.REFERENCE == 1);

            context.Produits.DeleteOnSubmit(produit);
            context.SubmitChanges();
        }
    }
}
